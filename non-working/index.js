/* This is our index file in javascript */

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res) {
        // Eerste stap stuur response
        //res.send('<h1>Hello stelletje gekkerds<h1>');
        //res.sendFile(__dirname+'/mywebpage.html');
        res.sendFile(__dirname+'/index.html');
});

// callback als er een client connectie is
// dit is zoiets als, ik wil reageren bij een 'connection'
io.on('connection', function(socket) {
        console.log('we got a new client connected');

        // we can also get event when client disconnects
        /*
        socket.on('disconnect', function(){
                console.log('user disconnected');
        });
        */
        socket.on('chatMessage', function(msg) {
                //console.log('Client wrote: '+msg);
                console.log('Client wrote something!');
        });
});


http.listen(3000, function() {
        console.log('listening on *:3000');
});

